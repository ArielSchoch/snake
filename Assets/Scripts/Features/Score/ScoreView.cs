using TMPro;
using UnityEngine;

namespace Snake.Features
{
    public class ScoreView : MonoBehaviour
    {
        public TextMeshProUGUI scoreText;

        public void UpdateScore(int score)
        {
            this.scoreText.text = $"Score: {score}";
        }
    }
}