namespace Snake.Features
{
    public class ScoreController
    {
        public ScoreController(ScoreModel model, ScoreView view)
        {
            model.Init();
            view.UpdateScore(0);
            Singletons.EventStream.Subscribe<UpdateScoreEvent>(EventStream.AnyCaller, evt => view.UpdateScore(evt.score));
        }
    }
}