namespace Snake.Features
{
    public class ScoreModel
    {
        public int Score { get; private set; }

        public void Init()
        {
            Singletons.EventStream.Subscribe<CollectBodyPartEvent>(EventStream.AnyCaller, evt =>
            {
                this.Score++;
                Singletons.EventStream.Publish(this, new UpdateScoreEvent { score = this.Score });
            });

            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, evt =>
            {
                this.Score = 0;
                Singletons.EventStream.Publish(this, new UpdateScoreEvent { score = this.Score });
            });
        }

        public void Reset()
        {
            this.Score = 0;
        }
    }
}