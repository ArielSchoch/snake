namespace Snake.Snake
{
    public class SnakeBodyController
    {
        public SnakeBodyModel model;
        public SnakeBodyView view;

        public SnakeBodyController(SnakeBodyModel model, SnakeBodyView view, Int2 startPos)
        {
            this.model = model;
            this.view = view;

            this.model.Init();
            this.model.UpdatePosition(startPos);
            this.view.SetPosition(startPos);

            Singletons.EventStream.Subscribe<MoveEvent>(model, OnMove);
            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, OnRestart);
        }

        private void OnMove(MoveEvent evt)
        {
            this.view.SetPosition(evt.position);
        }

        private void OnRestart(RestartEvent evt)
        {
            Cleanup();
        }

        private void Cleanup()
        {
            Singletons.EventStream.Unsubscribe<MoveEvent>(this.model, OnMove);
            Singletons.EventStream.Unsubscribe<RestartEvent>(EventStream.AnyCaller, OnRestart);

            UnityEngine.Object.Destroy(this.view.gameObject);

            this.model.Cleanup();
            this.model = null;
            this.view = null;
        }
    }
}