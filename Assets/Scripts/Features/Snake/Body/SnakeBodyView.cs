using UnityEngine;

namespace Snake.Snake
{
    public class SnakeBodyView : MonoBehaviour
    {
        public void SetPosition(Int2 gridPos)
        {
            var newPos = Singletons.Services.gridService.ToScreenPosition(gridPos);
            var pos = this.transform.position;
            pos.x = newPos.x;
            pos.y = newPos.y;
            this.transform.position = pos;
        }
    }
}