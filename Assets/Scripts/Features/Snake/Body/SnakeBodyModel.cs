
namespace Snake.Snake
{
    public class SnakeBodyModel
    {
        public Int2 Pos { get; private set; }

        public void Init()
        {
            Singletons.EventStream.Subscribe<SnakeStepEvent>(EventStream.AnyCaller, OnSnakeStep);
        }

        private void OnSnakeStep(SnakeStepEvent evt)
        {
            if (this.Pos == evt.position)
                Singletons.EventStream.Publish(EventStream.AnyCaller, new CrashEvent());
        }

        public void UpdatePosition(Int2 pos)
        {
            this.Pos = pos;
            Singletons.EventStream.Publish(this, new MoveEvent { position = pos });
        }

        public void Cleanup()
        {
            Singletons.EventStream.Unsubscribe<SnakeStepEvent>(EventStream.AnyCaller, OnSnakeStep);
        }
    }
}