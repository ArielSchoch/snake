using UnityEngine;

namespace Snake.Snake
{
    public class SnakeHeadView : MonoBehaviour
    {
        // Move time step logic to business logic
        public float timeStep = 0.1f;

        private float lastMoveUpdateTime = 0f;
        private readonly ChangeDirectionEvent changeDirectionEvent = new ChangeDirectionEvent();
        private readonly TickEvent tickEvent = new TickEvent();

        public void SetPosition(Int2 gridPos)
        {
            var newPos = Singletons.Services.gridService.ToScreenPosition(gridPos);
            var pos = this.transform.position;
            pos.x = newPos.x;
            pos.y = newPos.y;
            this.transform.position = pos;
        }

        private void Update()
        {
            if (Singletons.Services.gameService.IsPaused)
                return;

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                this.changeDirectionEvent.direction = GridDirection.Right;
                Singletons.EventStream.Publish(this, this.changeDirectionEvent);
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                this.changeDirectionEvent.direction = GridDirection.Left;
                Singletons.EventStream.Publish(this, this.changeDirectionEvent);
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                this.changeDirectionEvent.direction = GridDirection.Up;
                Singletons.EventStream.Publish(this, this.changeDirectionEvent);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                this.changeDirectionEvent.direction = GridDirection.Down;
                Singletons.EventStream.Publish(this, this.changeDirectionEvent);
            }

            if (Time.time - this.lastMoveUpdateTime >= this.timeStep)
            {
                this.lastMoveUpdateTime = Time.time;
                Singletons.EventStream.Publish(this, this.tickEvent);
            }
        }
    }
}