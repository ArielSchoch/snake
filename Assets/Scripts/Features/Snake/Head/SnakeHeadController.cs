namespace Snake.Snake
{
    public class SnakeHeadController
    {
        public readonly SnakeHeadModel model;
        public readonly SnakeHeadView view;

        public SnakeHeadController(SnakeHeadModel model, SnakeHeadView view, Int2 startPos)
        {
            this.model = model;
            this.view = view;

            this.model.Init(startPos);
            this.view.SetPosition(startPos);

            Singletons.EventStream.Subscribe<ChangeDirectionEvent>(view, evt => this.model.ChangeDirection(evt.direction));
            Singletons.EventStream.Subscribe<TickEvent>(view, evt =>
            {
                model.MoveStep();
                view.SetPosition(model.Pos);
            });
        }
    }
}