﻿using System.Collections.Generic;
using UnityEngine;

namespace Snake.Snake
{
    public class SnakeHeadModel
    {
        private Int2 pos;
        public Int2 Pos { get; private set; }

        private Int2 startPos;
        private GridDirection direction;
        private GridDirection lastStepDirection;
        private readonly List<SnakeBodyModel> snakeBody = new List<SnakeBodyModel>();

        public void Init(Int2 position)
        {
            this.startPos = position;
            this.Pos = this.startPos;
            this.lastStepDirection = this.direction;
            Singletons.EventStream.Subscribe<CollectBodyPartEvent>(EventStream.AnyCaller, evt => AddNewSnakeBodyPart());
            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, evt => Reset());
        }

        public void AddNewSnakeBodyPart()
        {
            var lastPartPos = this.snakeBody.Count > 0 ? this.snakeBody[this.snakeBody.Count - 1].Pos : this.Pos;
            var controller = Singletons.Factories.snakeFactory.CreateSnakeBody(Singletons.Factories.snakeFactory.DefaultParent, lastPartPos);
            this.snakeBody.Add(controller.model);
        }

        public void Reset()
        {
            this.Pos = this.startPos;
            this.snakeBody.Clear();
            Singletons.EventStream.Publish(this, new SnakeStepEvent { position = this.Pos });
        }

        public void ChangeDirection(GridDirection dir)
        {
            var isValidDirection = false;
            switch (dir)
            {
                case GridDirection.Right:
                    isValidDirection = this.lastStepDirection != GridDirection.Left;
                    break;
                case GridDirection.Left:
                    isValidDirection = this.lastStepDirection != GridDirection.Right;
                    break;
                case GridDirection.Up:
                    isValidDirection = this.lastStepDirection != GridDirection.Down;
                    break;
                case GridDirection.Down:
                    isValidDirection = this.lastStepDirection != GridDirection.Up;
                    break;
            }

            if (isValidDirection)
                this.direction = dir;
        }

        public void MoveStep()
        {
            for (var i = this.snakeBody.Count - 1; i > 0; i--)
                this.snakeBody[i].UpdatePosition(this.snakeBody[i-1].Pos);

            if (this.snakeBody.Count > 0)
                this.snakeBody[0].UpdatePosition(this.Pos);

            var gridService = Singletons.Services.gridService;
            switch (this.direction)
            {
                case GridDirection.Right:
                    this.Pos += Int2.Right;
                    if (this.Pos.x > gridService.GridWidth / 2)
                        this.Pos = new Int2(-gridService.GridWidth / 2, this.Pos.y);
                    break;
                case GridDirection.Left:
                    this.Pos += Int2.Left;
                    if (this.Pos.x < -gridService.GridWidth / 2)
                        this.Pos = new Int2(gridService.GridWidth / 2, this.Pos.y);
                    break;
                case GridDirection.Up:
                    this.Pos += Int2.Up;
                    if (this.Pos.y > gridService.GridHeight / 2)
                        this.Pos = new Int2(this.Pos.x, -gridService.GridHeight / 2);
                    break;
                case GridDirection.Down:
                    this.Pos += Int2.Down;
                    if (this.Pos.y < -gridService.GridHeight / 2)
                        this.Pos = new Int2(this.Pos.x, gridService.GridHeight / 2);
                    break;
            }

            this.lastStepDirection = this.direction;

            Singletons.EventStream.Publish(this, new SnakeStepEvent { position = this.Pos });
        }
    }
}