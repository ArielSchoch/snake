using TMPro;
using UnityEngine;

namespace Snake.Features.GameOver
{
    public class GameOverView : MonoBehaviour
    {
        public TextMeshProUGUI gameOverText;

        private void Start()
        {
            Singletons.EventStream.Subscribe<CrashEvent>(EventStream.AnyCaller, evt => this.gameOverText.enabled = true);
            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, evt => this.gameOverText.enabled = false);
        }

        private void Update()
        {
            if (!Singletons.Services.gameService.IsGameOver)
                return;

            if (Input.GetKeyDown(KeyCode.Space))
                Singletons.EventStream.Publish(this, new TransitionStartEvent());
        }
    }
}