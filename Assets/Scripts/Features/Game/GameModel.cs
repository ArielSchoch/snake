using UnityEngine;

namespace Snake.Features
{
    public class GameModel
    {
        public void Init()
        {
            Singletons.EventStream.Subscribe<CrashEvent>(EventStream.AnyCaller, evt =>
            {
                Singletons.Services.gameService.IsPaused = true;
                Singletons.Services.gameService.IsGameOver = true;
            });

            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, evt =>
            {
                Singletons.Services.gameService.IsGameOver = false;
            });

            Singletons.EventStream.Subscribe<TransitionEndEvent>(EventStream.AnyCaller, evt =>
            {
                Singletons.Services.gameService.IsPaused = false;
            });

            Singletons.EventStream.Subscribe<CollectBodyPartEvent>(EventStream.AnyCaller, evt =>
            {
                var gridHeight = Singletons.Services.gridService.GridHeight / 2;
                var gridWidth = Singletons.Services.gridService.GridWidth / 2;
                var randomPos = new Int2(Random.Range(-gridWidth, gridWidth), Random.Range(-gridHeight, gridHeight));
                Singletons.Factories.collectableFactory.CreateCollectable(CollectableType.Food, null, randomPos);
            });
        }
    }
}