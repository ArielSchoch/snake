namespace Snake.Features
{
    public class GameController
    {
        private GameModel model;

        public GameController(GameModel model)
        {
            this.model = model;
            this.model.Init();
        }
    }
}