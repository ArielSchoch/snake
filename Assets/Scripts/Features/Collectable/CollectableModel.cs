using System;
using UnityEngine;

namespace Snake.Features
{
    public enum CollectableType
    {
        Food
    }

    public class CollectableModel
    {
        public CollectableType CollectableType { get; set; }
        public Int2 Pos { get; private set; }

        public void Init(Int2 startPos)
        {
            this.Pos = startPos;
            Singletons.EventStream.Subscribe<SnakeStepEvent>(EventStream.AnyCaller, OnSnakeStep);
        }

        private void OnSnakeStep(SnakeStepEvent evt)
        {
            if (evt.position == this.Pos)
                Collect();
        }

        private void Collect()
        {
            switch (this.CollectableType)
            {
                case CollectableType.Food:
                    Singletons.EventStream.Publish(this, new CollectBodyPartEvent());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Cleanup()
        {
            Singletons.EventStream.Unsubscribe<SnakeStepEvent>(EventStream.AnyCaller, OnSnakeStep);
        }

        ~CollectableModel()
        {
            // Debug.Log("CollectableModel destructor called.");
        }
    }
}