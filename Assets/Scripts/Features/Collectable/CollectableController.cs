using UnityEngine;

namespace Snake.Features
{
    public class CollectableController
    {
        private CollectableModel model;
        private CollectableView view;

        public CollectableController(CollectableModel model, CollectableView view, Int2 startPos)
        {
            this.model = model;
            this.view = view;

            this.model.Init(startPos);

            Singletons.EventStream.Subscribe<CollectBodyPartEvent>(this.model, OnCollected);
        }

        private void OnCollected(CollectBodyPartEvent evt)
        {
            Cleanup();
        }

        private void Cleanup()
        {
            Singletons.EventStream.Unsubscribe<CollectBodyPartEvent>(this.model, OnCollected);

            Object.Destroy(this.view.gameObject);

            this.model.Cleanup();
            this.model = null;
            this.view = null;
        }

        ~CollectableController()
        {
            // Debug.Log("CollectableController destructor called.");
        }
    }
}