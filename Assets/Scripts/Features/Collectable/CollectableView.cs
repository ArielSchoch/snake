using System;
using UnityEditor;
using UnityEngine;

namespace Snake.Features
{
    public class CollectableView : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;

        public void SetCollectableType(CollectableType type)
        {
            switch (type)
            {
                case CollectableType.Food:
                    this.spriteRenderer.sprite = Resources.Load<Sprite>("Sprites/Collectable_" + type);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}