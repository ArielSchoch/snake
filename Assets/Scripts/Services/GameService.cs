namespace Snake
{
    public interface IGameService
    {
        bool IsPaused { get; set; }
        bool IsGameOver { get; set; }
    }

    public class GameService : IGameService
    {
        public bool IsPaused { get; set; }
        public bool IsGameOver { get; set; }
    }
}