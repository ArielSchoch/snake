using UnityEngine;

namespace Snake
{
    public interface IGridService
    {
        int GridWidth { get; }
        int GridHeight { get; }
        Vector2 ToScreenPosition(Int2 pos);
        bool IsInBounds(Int2 pos);
    }

    public class GridService : IGridService
    {
        private readonly float gridSpacing;

        public GridService(float gridSpacing)
        {
            this.gridSpacing = gridSpacing;
        }

        public int GridWidth => (int) (Camera.main.orthographicSize * Camera.main.aspect / this.gridSpacing) * 2;
        public int GridHeight => (int) (Camera.main.orthographicSize / this.gridSpacing) * 2;

        public Vector2 ToScreenPosition(Int2 pos)
        {
            return new Vector2(pos.x * this.gridSpacing, pos.y * this.gridSpacing);
        }

        public bool IsInBounds(Int2 pos)
        {
            return Mathf.Abs(pos.x) <= (this.GridWidth / 2) && Mathf.Abs(pos.y) <= (this.GridHeight / 2);
        }
    }
}