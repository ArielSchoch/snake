using Snake.Features;
using UnityEngine;

namespace Snake
{
    public interface ICollectableFactory
    {
        CollectableController CreateCollectable(CollectableType type, Transform parent, Int2 position);
    }

    public class CollectableFactory : ICollectableFactory
    {
        private readonly GameObject collectablePrefab;

        public CollectableFactory(GameObject collectablePrefab)
        {
            this.collectablePrefab = collectablePrefab;
        }

        public CollectableController CreateCollectable(CollectableType type, Transform parent, Int2 position)
        {
            var screenPos = Singletons.Services.gridService.ToScreenPosition(position);
            var model = new CollectableModel { CollectableType = type };
            var instance = Object.Instantiate(this.collectablePrefab, screenPos, Quaternion.identity, parent);
            var view = instance.GetComponent<CollectableView>();
            view.SetCollectableType(type);
            var controller = new CollectableController(model, view, position);
            return controller;
        }
    }
}