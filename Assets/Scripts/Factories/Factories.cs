namespace Snake
{
    public class Factories
    {
        public ISnakeFactory snakeFactory;
        public ICollectableFactory collectableFactory;
    }
}