using Snake.Snake;
using UnityEngine;

namespace Snake
{
    public interface ISnakeFactory
    {
        Transform DefaultParent { get; }
        SnakeHeadController CreateSnakeHead(Transform parent, Int2 startPos);
        SnakeBodyController CreateSnakeBody(Transform parent, Int2 startPos);
    }

    public class SnakeFactory : ISnakeFactory
    {
        private readonly GameObject snakeHeadPrefab;
        private readonly GameObject snakeBodyPrefab;
        private readonly Color[] snakeColors;
        private static int snakeBodyIndex = 0;
        public Transform DefaultParent { get; }

        public SnakeFactory(GameObject snakeHeadPrefab, GameObject snakeBodyPrefab, Transform defaultParent, Color[] snakeColors)
        {
            this.snakeHeadPrefab = snakeHeadPrefab;
            this.snakeBodyPrefab = snakeBodyPrefab;
            this.snakeColors = snakeColors;
            this.DefaultParent = defaultParent;
        }

        public SnakeHeadController CreateSnakeHead(Transform parent, Int2 startPos)
        {
            var model = new SnakeHeadModel();
            var instance = Object.Instantiate(this.snakeHeadPrefab, parent);
            var view = instance.GetComponent<SnakeHeadView>();
            var controller = new SnakeHeadController(model, view, startPos);
            return controller;
        }

        public SnakeBodyController CreateSnakeBody(Transform parent, Int2 startPos)
        {
            var model = new SnakeBodyModel();
            var instance = Object.Instantiate(this.snakeBodyPrefab, parent);
            var view = instance.GetComponent<SnakeBodyView>();
            view.GetComponent<SpriteRenderer>().color = this.snakeColors[snakeBodyIndex % this.snakeColors.Length];
            snakeBodyIndex++;
            var controller = new SnakeBodyController(model, view, startPos);
            return controller;
        }
    }
}