using UnityEditor;
using UnityEngine;

namespace Snake.Effects
{
    public class TextureCreation : MonoBehaviour
    {
        [MenuItem("GameObject/Create Fade Texture")]
        public static void CreateTextureVariants()
        {
            CreateTexture(8);
            CreateTexture(12);
            CreateTexture(32);
            CreateTexture(64);
        }

        public static void CreateTexture(int size)
        {
            var texture = new Texture2D(size, size, TextureFormat.ARGB32, false);

            for (var x = 0; x < texture.width; x++)
            {
                for (var y = 0; y < texture.height; y++)
                {
                    var brightness = (x * texture.height + y) / (float)(texture.width * texture.height);
                    texture.SetPixel(x, y, new Color(brightness, brightness, brightness, 1f));
                }
            }

            texture.Apply();

            var path = Application.dataPath + $"/Images/FadeCol{texture.width}x{texture.height}.png";
            SaveTextureAsPNG(texture, path);
            Debug.Log(path);
        }

        public static void SaveTextureAsPNG(Texture2D texture, string fullPath)
        {
            byte[] bytes = texture.EncodeToPNG();
            System.IO.File.WriteAllBytes(fullPath, bytes);
        }
    }
}