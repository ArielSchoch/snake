using System.Collections;
using UnityEngine;

namespace Snake.Effects
{
    public class TransitionEffect : MonoBehaviour
    {
        public Material material;
        public float duration = 1f;
        public Texture2D fadeOutTexture;
        public Texture2D fadeInTexture;

        private static readonly int thresholdId = Shader.PropertyToID("_Threshold");
        private static readonly int transitionTexId = Shader.PropertyToID("_TransitionTex");

        private void Start()
        {
            SetThreshold(0f);

            Singletons.EventStream.Subscribe<TransitionStartEvent>(EventStream.AnyCaller, evt =>
            {
                StartCoroutine(ShowTransitionEffect());
            });
        }

        private IEnumerator ShowTransitionEffect()
        {
            SetFadeTexture(this.fadeOutTexture);

            float transitionStartTime = Time.time;
            while (Time.time < transitionStartTime + this.duration)
            {
                var completion = Mathf.InverseLerp(transitionStartTime, transitionStartTime + this.duration, Time.time);
                SetThreshold(completion);
                yield return null;
            }

            SetThreshold(1f);
            SetFadeTexture(this.fadeInTexture);
            Singletons.EventStream.Publish(this, new RestartEvent());

            transitionStartTime = Time.time;
            while (Time.time < transitionStartTime + this.duration)
            {
                var completion = Mathf.InverseLerp(transitionStartTime + this.duration, transitionStartTime, Time.time);
                SetThreshold(completion);
                yield return null;
            }

            SetThreshold(0f);
            Singletons.EventStream.Publish(this, new TransitionEndEvent());
        }

        private void SetThreshold(float threshold)
        {
            this.material.SetFloat(thresholdId, threshold);
        }

        private void SetFadeTexture(Texture2D tex)
        {
            this.material.SetTexture(transitionTexId, tex);
        }

        public void OnRenderImage(RenderTexture src, RenderTexture dest)
        {
            Graphics.Blit(src, dest, this.material);
        }
    }
}