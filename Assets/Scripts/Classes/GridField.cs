using UnityEngine;

namespace Snake
{
    public enum GridDirection
    {
        Left, Right, Up, Down
    }

    public struct Int2
    {
        public readonly int x;
        public readonly int y;

        public Int2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static bool operator == (Int2 a, Int2 b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator != (Int2 a, Int2 b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public static Int2 operator + (Int2 a, Int2 b)
        {
            return new Int2(a.x + b.x, a.y + b.y);
        }

        public static Int2 operator - (Int2 a, Int2 b)
        {
            return new Int2(a.x - b.x, a.y - b.y);
        }

        public static explicit operator Vector2(Int2 pos)
        {
            return new Vector2(pos.x, pos.y);
        }

        public static explicit operator Vector3(Int2 pos)
        {
            return new Vector3(pos.x, pos.y, 0f);
        }

        public bool Equals(Int2 other)
        {
            return this.x == other.x && this.y == other.y;
        }

        public override bool Equals(object obj)
        {
            return obj is Int2 other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.x * 397) ^ this.y;
            }
        }

        public static readonly Int2 Zero = new Int2(0, 0);
        public static readonly Int2 One = new Int2(1, 1);
        public static readonly Int2 Right = new Int2(1, 0);
        public static readonly Int2 Left = new Int2(-1, 0);
        public static readonly Int2 Up = new Int2(0, 1);
        public static readonly Int2 Down = new Int2(0, -1);
    }
}