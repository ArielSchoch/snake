namespace Snake
{
    public class ChangeDirectionEvent : IEvent
    {
        public GridDirection direction;
    }

    public class TickEvent : IEvent {}

    public class SnakeStepEvent : IEvent
    {
        public Int2 position;
    }

    public class MoveEvent : IEvent
    {
        public Int2 position;
    }

    public class CollectBodyPartEvent : IEvent {}

    public class CrashEvent : IEvent {}

    public class UpdateScoreEvent : IEvent
    {
        public int score;
    }

    public class RestartEvent : IEvent {}

    public class TransitionStartEvent : IEvent {}

    public class TransitionEndEvent : IEvent {}
}