using System;
using System.Collections.Generic;

/*
Example Usage:
EventStream.Subscribe<MoveEvent>(EventStream.AnyCaller, evt => {});
EventStream.Publish(this, new MoveEvent());
*/

public interface IEvent {}

public interface IEventKeeper
{
    void Publish(object evt);
}

public class EventKeeper<TEvent> : IEventKeeper
{
    public Action<TEvent> action;

    public void Publish(object evt)
    {
        this.action?.Invoke((TEvent)evt);
    }
}

public interface IEventStream
{
    void Subscribe<TEvent>(object caller, Action<TEvent> handler) where TEvent : IEvent;
    void Unsubscribe<TEvent>(object caller, Action<TEvent> handler) where TEvent : IEvent;
    void Publish<TEvent>(object caller, TEvent evt) where TEvent : IEvent;
}

public class EventStream : IEventStream
{
    public static readonly object AnyCaller = new object();
    private readonly Dictionary<Type, Dictionary<object, IEventKeeper>> events = new Dictionary<Type, Dictionary<object, IEventKeeper>>();

    public void Subscribe<TEvent>(object caller, Action<TEvent> handler) where TEvent : IEvent
    {
        // Ensure that dictionary exists for this event type
        if (!this.events.TryGetValue(typeof(TEvent), out var eventDictionary))
        {
            eventDictionary = new Dictionary<object, IEventKeeper>();
            this.events.Add(typeof(TEvent), eventDictionary);
        }

        // Ensure that eventKeeper exists for this caller
        if (!eventDictionary.TryGetValue(caller, out var eventKeeper))
        {
            eventKeeper = new EventKeeper<TEvent>();
            eventDictionary.Add(caller, eventKeeper);
        }

        (eventKeeper as EventKeeper<TEvent>).action += handler;
    }

    public void Unsubscribe<TEvent>(object caller, Action<TEvent> handler) where TEvent : IEvent
    {
        if (!this.events.TryGetValue(typeof(TEvent), out var eventDictionary))
            return;

        if (!eventDictionary.TryGetValue(caller, out var eventKeeper))
            return;

        (eventDictionary[caller] as EventKeeper<TEvent>).action -= handler;

        // Release any objects which are not subscribed anymore
        var action = (eventDictionary[caller] as EventKeeper<TEvent>).action;
        if (action == null || action.GetInvocationList().Length == 0)
            eventDictionary.Remove(caller);
    }

    public void Publish<TEvent>(object caller, TEvent evt) where TEvent : IEvent
    {
        if (!this.events.TryGetValue(evt.GetType(), out var eventDictionary))
            return;

        // Notify all listeners which don't care about the caller
        if (eventDictionary.ContainsKey(AnyCaller))
            eventDictionary[AnyCaller].Publish(evt);

        if (!eventDictionary.TryGetValue(caller, out var eventKeeper))
            return;

        // Notify specific listeners
        if (caller != AnyCaller)
            eventDictionary[caller].Publish(evt);
    }
}