using System;
using Snake.Features;
using Snake.Snake;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Snake
{
    [Serializable]
    public class GameConfiguration
    {
        public GameObject snakeHeadPrefab;
        public GameObject snakeBodyPrefab;
        public Transform snakeParent;
        public GameObject collectablePrefab;
        public ScoreView scoreView;
        public int snakeStartLength = 10;
        public float gridSpacing = 0.2f;
        public Color[] snakeColors;
    }

    public class Main : MonoBehaviour
    {
        public GameConfiguration gameConfiguration;

        private GameController gameController;
        private SnakeHeadController snakeHeadController;
        private ScoreController scoreController;

        public void Awake()
        {
            LoadGame(this.gameConfiguration);
        }

        private void LoadGame(GameConfiguration gameConfig)
        {
            var factories = new Factories
            {
                snakeFactory = new SnakeFactory(gameConfig.snakeHeadPrefab, gameConfig.snakeBodyPrefab, gameConfig.snakeParent, gameConfig.snakeColors),
                collectableFactory = new CollectableFactory(gameConfig.collectablePrefab)
            };

            var services = new Services
            {
                gridService = new GridService(gameConfig.gridSpacing),
                gameService = new GameService()
            };

            Singletons.Factories = factories;
            Singletons.Services = services;
            Singletons.EventStream = new EventStream();

            var gameModel = new GameModel();
            this.gameController = new GameController(gameModel);

            var scoreModel = new ScoreModel();
            this.scoreController = new ScoreController(scoreModel, gameConfig.scoreView);

            this.snakeHeadController = factories.snakeFactory.CreateSnakeHead(gameConfig.snakeParent, new Int2(0, 0));

            AddStartSnakeLength(gameConfig.snakeStartLength);

            var gridHeight = Singletons.Services.gridService.GridHeight / 2;
            var gridWidth = Singletons.Services.gridService.GridWidth / 2;
            var randomPos = new Int2(Random.Range(-gridWidth, gridWidth), Random.Range(-gridHeight, gridHeight));
            Singletons.Factories.collectableFactory.CreateCollectable(CollectableType.Food, null, randomPos);

            Singletons.EventStream.Subscribe<RestartEvent>(EventStream.AnyCaller, evt => AddStartSnakeLength(gameConfig.snakeStartLength));
        }

        private void AddStartSnakeLength(int length)
        {
            for (var i = 0; i < length; i++)
                this.snakeHeadController.model.AddNewSnakeBodyPart();
        }
    }
}