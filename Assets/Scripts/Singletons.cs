namespace Snake
{
    public class Singletons
    {
        public static Factories Factories { get; set; }
        public static Services Services { get; set; }
        public static IEventStream EventStream { get; set; }
    }
}